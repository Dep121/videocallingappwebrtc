 /* exported trace */

// Logging utility function.
var server_messages = document.getElementById("server_messages");
function trace(arg) {
  var now = (window.performance.now() / 1000).toFixed(3);
  console.log(now + ': ', arg);
  server_messages.innerHTML += "<br>"+now + ': ' + arg;
}