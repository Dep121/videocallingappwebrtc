var signalingChannel = new WebSocket("wss://192.168.43.88:8080");
// var signalingChannel = new WebSocket("ws://192.168.43.88:8080");

var STUN = {
    urls: 'stun:stun.l.google.com:19302'
};

var TURN = {
    urls: 'turn:turn.bistri.com:80',
    credential: 'homeo',
    username: 'homeo'
};

var configuration = { iceServers: [STUN, TURN] };

signalingChannel.onopen = function(){
  trace("websocket open ok!! code: " + event.code);
}

signalingChannel.onclose = function(){
  trace("websocket closed ok!! code: " + event.code +" event: "+event);
}

signalingChannel.onerror = function(event){
  trace("websocket error !! code: " + event.code +" event: "+event);
}

signalingChannel.onmessage = function (evt) {
  trace("message arrived" + evt.data);

    var message = JSON.parse(evt.data);
    if (message.type == "addIce") {
      trace("addIce arrived");
      addCandidate(message.candidate);
    } else if (message.type == "offer_desc") {
      trace("offer desc arrived");
      offerButton.disabled = true;
      answerButton.disabled = false;
      remoteDescription(message.remote_desc);
    } else if (message.type == "answer_desc") {
      trace("answer desc arrived");
      remoteDescription(message.remote_desc);
    }
}

var startButton = document.getElementById('startButton');
var callButton = document.getElementById('callButton');
var hangupButton = document.getElementById('hangupButton');
var offerButton = document.getElementById('offerButton');
var answerButton = document.getElementById('answerButton');
callButton.disabled = true;
offerButton.disabled = true;
answerButton.disabled = true;
hangupButton.disabled = true;
startButton.onclick = start;
callButton.onclick = call;
offerButton.onclick = offer;
answerButton.onclick = answer;
hangupButton.onclick = hangup;

var startTime;
var localVideo = document.getElementById('localVideo');
var remoteVideo = document.getElementById('remoteVideo');

localVideo.addEventListener('loadedmetadata', function() {
  trace('Local video videoWidth: ' + this.videoWidth +
    'px,  videoHeight: ' + this.videoHeight + 'px');
});

remoteVideo.addEventListener('loadedmetadata', function() {
  trace('Remote video videoWidth: ' + this.videoWidth +
    'px,  videoHeight: ' + this.videoHeight + 'px');
});

remoteVideo.onresize = function() {
  trace('Remote video size changed to ' +
    remoteVideo.videoWidth + 'x' + remoteVideo.videoHeight);
  // We'll use the first onsize callback as an indication that video has started
  // playing out.
  if (startTime) {
    var elapsedTime = window.performance.now() - startTime;
    trace('Setup time: ' + elapsedTime.toFixed(3) + 'ms');
    startTime = null;
  }
};

var localStream;
var pc1;
var pc2;
var offerOptions = {
  offerToReceiveAudio: 1,
  offerToReceiveVideo: 1
};

function getName(pc) {
  return (pc === pc1) ? 'pc1' : 'pc2';
}

// function getOtherPc(pc) {
//   return (pc === pc1) ? pc2 : pc1;
// }

function gotStream(stream) {
  trace('Received local stream');
  localVideo.srcObject = stream;
  localStream = stream;
  callButton.disabled = false;
}

function start() {
  trace('Requesting local stream');
  startButton.disabled = true;
  navigator.mediaDevices.getUserMedia({
    audio: true,
    video: true
  })
  .then(gotStream)
  .catch(function(e) {
    alert('getUserMedia() error: ' + e.name);
  });

  signalingChannel.send(JSON.stringify({ "type" : "join", "token" : 0}));
}

function call() {
  callButton.disabled = true;
  hangupButton.disabled = false;
  trace('Starting call');
  startTime = window.performance.now();
  var videoTracks = localStream.getVideoTracks();
  var audioTracks = localStream.getAudioTracks();
  if (videoTracks.length > 0) {
    trace('Using video device: ' + videoTracks[0].label);
  }
  if (audioTracks.length > 0) {
    trace('Using audio device: ' + audioTracks[0].label);
  }
  // var servers = null;
  pc1 = new RTCPeerConnection(configuration);                             //notice
  trace('Created local peer connection object pc1');
  pc1.onicecandidate = function(e) {
    signalingChannel.send(JSON.stringify({ "type" : "addIce", "candidate" : e.candidate, "token" : 0}));
  };
  // pc2 = new RTCPeerConnection(servers);
  // trace('Created remote peer connection object pc2');
  // pc2.onicecandidate = function(e) {
  //   onIceCandidate(pc2, e);
  // };
  pc1.oniceconnectionstatechange = function(e) {
    onIceStateChange(pc1, e);
  };
  // pc2.oniceconnectionstatechange = function(e) {
  //   onIceStateChange(pc2, e);
  // };
  pc1.ontrack = gotRemoteStream;

  localStream.getTracks().forEach(
    function(track) {
      pc1.addTrack(
        track,
        localStream
      );
    }
  );
  trace('Added local stream to pc1');
  offerButton.disabled = false;
}

function offer(){
  trace('pc1 createOffer start');
  pc1.createOffer(
    offerOptions
  ).then(
    onCreateOfferSuccess,
    onCreateSessionDescriptionError
  );
}

function onCreateSessionDescriptionError(error) {
  trace('Failed to create session description: ' + error.toString());
}

function onCreateOfferSuccess(desc) {
  trace('Offer from pc1\n' + desc.sdp);
  trace('pc1 setLocalDescription start');
  pc1.setLocalDescription(desc).then(
    function() {
      signalingChannel.send(JSON.stringify({ "type" : "offer_desc", "remote_desc" : desc, "token" : 0}));
      onSetLocalSuccess(pc1);
    },
    onSetSessionDescriptionError
  );
}

function remoteDescription(desc){
  trace('pc1 setRemoteDescription start');
  pc1.setRemoteDescription(desc).then(
    function() {
      onSetRemoteSuccess(pc1);
    },
    onSetSessionDescriptionError
  );
}

function answer(){
  trace('pc2 createAnswer start');
  // Since the 'remote' side has no media stream we need
  // to pass in the right constraints in order for it to
  // accept the incoming offer of audio and video.
  pc1.createAnswer().then(
    onCreateAnswerSuccess,
    onCreateSessionDescriptionError
  );
}

function onSetLocalSuccess(pc) {
  trace(getName(pc) + ' setLocalDescription complete');
}

function onSetRemoteSuccess(pc) {
  trace(getName(pc) + ' setRemoteDescription complete');
}

function onSetSessionDescriptionError(error) {
  trace('Failed to set session description: ' + error.toString());
}

function gotRemoteStream(e) {
  if (remoteVideo.srcObject !== e.streams[0]) {
    remoteVideo.srcObject = e.streams[0];
    trace('pc1 received remote stream');
  }
}

function onCreateAnswerSuccess(desc) {
  trace('Answer from pc2:\n' + desc.sdp);
  trace('pc2 setLocalDescription start');
  pc1.setLocalDescription(desc).then(
    function() {
      signalingChannel.send(JSON.stringify({ "type" : "answer_desc", "remote_desc" : desc, "token" : 0}));
      onSetLocalSuccess(pc1);
    },
    onSetSessionDescriptionError
  );
}

function addCandidate(candidate) {
  pc1.addIceCandidate(candidate)
  .then(
    function() {
      onAddIceCandidateSuccess(pc1);
    },
    function(err) {
      onAddIceCandidateError(pc1, err);
    }
  );
  trace(getName(pc1) + ' ICE candidate: \n' + (candidate ?
      candidate.candidate : '(null)'));
}

function onAddIceCandidateSuccess(pc) {
  trace(getName(pc) + ' addIceCandidate success');
}

function onAddIceCandidateError(pc, error) {
  trace(getName(pc) + ' failed to add ICE Candidate: ' + error.toString());
}

function onIceStateChange(pc, event) {
  if (pc) {
    trace(getName(pc) + ' ICE state: ' + pc.iceConnectionState);
    console.log('ICE state change event: ', event);
  }
}

function hangup() {
  trace('Ending call');
  pc1.close();
  // pc2.close();
  pc1 = null;
  // pc2 = null;
  hangupButton.disabled = true;
  callButton.disabled = false;
}
