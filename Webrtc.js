var https = require('https');
// var https = require('http');
var url = require('url');
var fs = require('fs');
var websocket = require('websocket').server;
var port = 8080;
var webrtc_clients = [];
var webrtc_discussions = {};


var page = undefined;
fs.readFile("basic_video_call.html", function(error, data){
	if(error) {
		log_error(error);
	} else {
		page = data;
	}
});

var page_css = undefined;
fs.readFile("./css/basic_video.css", function(error, data){
	if(error) {
		log_error(error);
	} else {
		page_css = data;
	}
});

var page_adapter_js = undefined;
var page_common_js = undefined;
var page_main_js = undefined;
var page_ga_js = undefined;
fs.readFile("./js/adapter-latest.js", function(error, data){
	if(error) {
		log_error(error);
	} else {
		page_adapter_js = data;
	}
});
fs.readFile("./js/common.js", function(error, data){
	if(error) {
		log_error(error);
	} else {
		page_common_js = data;
	}
});
fs.readFile("./js/main.js", function(error, data){
	if(error) {
		log_error(error);
	} else {
		page_main_js = data;
	}
});
fs.readFile("./js/ga.js", function(error, data){
	if(error) {
		log_error(error);
	} else {
		page_ga_js = data;
	}
});

const options = {
  key: fs.readFileSync('public_private_key/key_webrtc.key'),
  cert: fs.readFileSync('public_private_key/crt_webrtc.crt')
};

var https_server = https.createServer(options, function (request, response) {
// var https_server = https.createServer(function (request, response) {
  // response.writeHead(200, {'Content-Type': 'text/html'});
  // var q = url.parse(req.url, true).query;
  // var txt = q.year + " " + q.month;
  // res.end(txt);

  if (request.url.indexOf('.css') != -1) {
  	response.writeHead(200, {'Content-Type': 'text/css'});
  	response.write(page_css);
  	response.end();
  	log_comment("css sent ok!!");
  	return;
  }

  if (request.url.indexOf('.js') != -1) {
  	response.writeHead(200, {'Content-Type': 'text/javascript'});
  	response.write(page_adapter_js);
  	response.write(page_ga_js);
  	response.write(page_main_js);
  	response.write(page_common_js);
  	response.end();
  	log_comment("js sent ok!!");
  	return;
  }

  response.writeHead(200, {'Content-Type': 'text/html'});
  response.write(page);
  response.end();
});

https_server.listen(port, function () {
	log_comment("server listening (port "+port+")");
});

//web socket functions

var websocket_server = new websocket({
	httpServer: https_server
});

// https_server.on('upgrade', websocket_server.handleUpgrade);

websocket_server.on("request", function(request) {
	log_comment("new request ("+request.origin+")");

	var connection = request.accept(null, request.origin);
	

	webrtc_clients.push(connection);
	connection.id = webrtc_clients.length-1;

	log_comment("new connection (id="+connection.id+") ("+connection.remoteAddress+")");

	connection.on("message", function(message) {
		if(message.type === "utf8") {
			log_comment("got message "+message.utf8Data);

			var signal = undefined;
			try { signal = JSON.parse(message.utf8Data); } catch(e) {};

			if (signal) {
				if (signal.type === "join" && signal.token !== undefined) {
					log_comment("join and token 0 and "+signal.token+" and id="+connection.id+"ok!!");
					// connection.send("hello",log_error);
					try {
						if (webrtc_discussions[signal.token] === undefined) {
							webrtc_discussions[signal.token] = {};
							log_comment("signal.token = {"+signal.token+"} ok!!");
						}
					} catch(e) { };
					try {
						webrtc_discussions[signal.token][connection.id] = true;
						log_comment("before notification ok!!");
						// notify(connection.id, connection.remoteAddress, true);
					} catch(e) { };
				} else if (signal.token !== undefined) {
					try {
						Object.keys(webrtc_discussions[signal.token]).
						forEach(function(id) {
							if (id != connection.id) {
								webrtc_clients[id].send(message.utf8Data,
									log_error);
								log_comment("send message to id="+id);
							}
						});
					} catch(e) { };
				} else {
					log_comment("invalid signal 1: "+message.utf8Data);
				}
			} else {
				log_comment("invalid signal 2: "+message.utf8Data);
			}
		}
	});

	connection.on("close", function(connection) {
		log_comment("connection closed "+connection.remoteAddress+")");
		// notify(connection.id, connection.remoteAddress, false);
		Object.keys(webrtc_discussions).forEach(function(token) {
			Object.keys(webrtc_discussions[token]).forEach(function(id) {
				if (id === connection.id) {
					delete webrtc_discussions[token][id];
				}
			});
		});
	});
});


//utility functions
function log_error(error) {
	if (error !== "Connection closed" && error !== undefined) {
		log_comment("ERROR: "+error);
	}
}

function log_comment(comment) {
	console.log((new Date())+" "+comment);
}

var msg = {
	"notification" : {}
};

function notify(id, address, entered) {
	log_comment("after notification ok!!");
	if (entered) {
		msg["notification"]["type"] = "entered";
		log_comment("after notification entered ok!!");
		Object.keys(webrtc_discussions).forEach(function(token) {
			log_comment("token = "+ JSON.stringify(token));
			Object.keys(webrtc_discussions[token]).forEach(function(i) {
				log_comment("tokenids = "+(i+1) +" and yourid= "+(id+1));
				if (i != id) {
					msg["notification"]["id"] = id;
					msg["notification"]["address"] = address;
					webrtc_clients[i].send(JSON.stringify(msg), log_error);
					log_comment("notification sent i != id entered");
				}else{
					log_comment("success " + webrtc_clients["0"]);
					msg["notification"]["id"] = id;
					msg["notification"]["address"] = "you";
					webrtc_clients[i].send(JSON.stringify(msg), log_error);
					log_comment("notification sent else entered");
				}
			});
		});
	} else {
		msg["notification"]["type"] = "left";
		Object.keys(webrtc_discussions).forEach(function(token) {
			Object.keys(webrtc_discussions[token]).forEach(function(i) {
					msg["notification"]["id"] = id;
					webrtc_clients[i].send(JSON.stringify(msg), log_error);
					log_comment("notification sent left");
			});
		});
	}
}